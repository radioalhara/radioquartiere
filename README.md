# عائلة راديوهات يامكان

This is the repository for the YaMakan family of radios, which includes 5 stations so far. 

Each station gets its own folder. For now, everything is edited manually in html. 


## Some tips

### Recording output

To record your radio station, you can use the VLC command line tools. Here's the command that I'm using for Radio Il Hai:

```vlc -vvv http://stream.radiojar.com/77mh9vd3rtzuv --sout "#transcode{vcodec=none,acodec=vorb,ab=128,channels=2,samplerate=44100}:file{dst=radio_il_hai_$(date +%Y-%m-%d-%H:%M).ogg}"```